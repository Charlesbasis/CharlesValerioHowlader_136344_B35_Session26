<?php


namespace App\Gender;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Gender extends DB{

    public $id="";

    public $name="";

    public $gender="";



    public function __construct(){

        parent::__construct();

    }

    public function index(){
        echo "Gender found!";
    }
    public function setData($postVariabledata=NULL){

        if(array_key_exists('id',$postVariabledata)){
            $this->id = $postVariabledata['id'];
        }

        if(array_key_exists('name',$postVariabledata)){
            $this->name = $postVariabledata['name'];
        }

        if(array_key_exists('gender',$postVariabledata)){
            $this->gender = $postVariabledata['gender'];
        }
    }

    public function store(){

        $arrData = array($this->name, $this->gender);

        $sql = "Insert INTO gender(name,gender) VALUES(?,?)";


        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully! :)");
        else
            Message::message("Failed! Data Has Not Been Inserted! :(");

        Utility::redirect('create.php');

    } //end of store method

}

